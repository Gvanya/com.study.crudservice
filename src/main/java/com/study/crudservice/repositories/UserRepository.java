package com.study.crudservice.repositories;

import com.study.crudservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by igerasymchuk on 9/25/2018.
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
}
