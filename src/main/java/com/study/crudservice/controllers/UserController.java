package com.study.crudservice.controllers;

import com.study.crudservice.Services.UserService;
import com.study.crudservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by igerasymchuk on 9/25/2018.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public String showUsers (Model model){
        model.addAttribute("users", userService.getUsers());
        return "users";
    }

    @RequestMapping(value = "/user{id}")
    public String showUser(Model model, @RequestParam("id") int id) {
        model.addAttribute("users", userService.getUserById(id));
        return "users";
    }

    @RequestMapping(value ="/delete")
    public String deleteUser(Model model, @RequestParam("id") int id){
        userService.deleteUserById(id);
        model.addAttribute("users", userService.getUsers());
        return "users";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createPage(Model model) {
        model.addAttribute("user", new User());
        return "create";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String addedUser(@ModelAttribute User user, Model model) {
        userService.createUser(user);
        model.addAttribute("users", userService.getUsers());
        return "users";
    }

}
