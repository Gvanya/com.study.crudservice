package com.study.crudservice.Services;

import com.study.crudservice.model.User;
import com.study.crudservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * Created by igerasymchuk on 9/25/2018.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @Override
    public User getUserById(@PathVariable long id)  {
        Optional<User> user = userRepository.findById(id);
        return user.get();
    }

    @Override
    public void deleteUserById(@PathVariable long id)  {
        userRepository.deleteById(id);
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

}
