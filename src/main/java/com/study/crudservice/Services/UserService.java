package com.study.crudservice.Services;

import com.study.crudservice.model.User;

import java.util.List;

/**
 * Created by igerasymchuk on 9/25/2018.
 */
public interface UserService {

    List<User> getUsers();
    User getUserById(long id);
    void deleteUserById( long id);
    User createUser(User user);
}
